/**
 * @param {string} haystack
 * @param {string} needle
 * @return {number}
 */
var strStr_1 = function(haystack, needle) {
    return haystack.indexOf(needle, 0);  
};

var strStr_2 = function(haystack, needle) {
    if (needle.length == 0) return 0;
    let len = needle.length;
    for (let i = 0; i <= haystack.length - len; i++) {
        let subStr = haystack.substring(i, i + len);
        if (subStr == needle) return i;
    } 
    return -1;
};

console.log(strStr_2("helloKeloo", "lle"));
// console.log(strStr_2("hello", ""));
// console.log(strStr_2("aaaaa", "bba"));