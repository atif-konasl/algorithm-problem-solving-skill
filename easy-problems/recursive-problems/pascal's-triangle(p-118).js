/**
 * @param {number} numRows
 * @return {number[][]}
 * 
 * Given a non-negative integer numRows, generate the first numRows of Pascal's triangle.


In Pascal's triangle, each number is the sum of the two numbers directly above it.

Example:

Input: 5
Output:
[
     [1],
    [1,1],
   [1,2,1],
  [1,3,3,1],
 [1,4,6,4,1]
]
 */
var generate = function(numRows) {
    if (numRows == 0) return [];
    let arr = [[1]];
    if (numRows == 1) return arr;
    arr.push([1,1]);
    for (let i = 2; i < numRows; i++) {
        let temp = [];
        temp.push(1);
        for (let j = 1; j < i; j++) {
            temp.push(arr[i-1][j-1] + arr[i-1][j]);
        }
        temp.push(1);
        arr.push(temp);
        console.log(temp);
    }
    return arr;
};

console.log (generate (0));