/**
 * @param {number[]} nums1
 * @param {number} m
 * @param {number[]} nums2
 * @param {number} n
 * @return {void} Do not return anything, modify nums1 in-place instead.
 * 
 * Given two sorted integer arrays nums1 and nums2, merge nums2 into nums1 as one sorted array.

Note:

The number of elements initialized in nums1 and nums2 are m and n respectively.
You may assume that nums1 has enough space (size that is greater or equal to m + n) to hold additional elements from nums2.
Example:

Input:
nums1 = [1,2,3,0,0,0], m = 3
nums2 = [2,5,6],       n = 3

Output: [1,2,2,3,5,6]
 */
var merge = function(nums1, m, nums2, n) {
    nums1.splice(m, n);
    let corrPosition = 0;
    for (let i = 0; i < n; i++) {
        corrPosition = binarySearch(nums1, corrPosition, nums1.length - 1, nums2[i]);
        nums1.splice(corrPosition, 0, nums2[i]);
        // console.log(nums1);
    }
    return nums1;
};

var binarySearch = (arr, left, right, item) => {
    // console.log(arr, left, right, item);
    while (left <= right) {
        let mid = parseInt((left + right) / 2);
        if (arr[mid] > item) {
            right = mid - 1;
        } else if (arr[mid] < item) {
            left = mid + 1;
        } else {
            return mid;
        }
    }
    return left;
}


console.log(merge([6,8,13,0,0,0], 3, [2,5,6], 3));
console.log(merge([6,8,13,0,0,0], 3, [2,15,26], 3));