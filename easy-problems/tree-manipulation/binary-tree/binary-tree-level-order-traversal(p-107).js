/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number[][]}
 * 
 * Given a binary tree, return the bottom-up level order traversal of its nodes' values. (ie, from left to right, level by level from leaf to root).

For example:
Given binary tree [3,9,20,null,null,15,7],
    3
   / \
  9  20
    /  \
   15   7
return its bottom-up level order traversal as:
[
  [15,7],
  [9,20],
  [3]
]
 */


var levelOrderBottom = function(root) {
    
    if (!root) return root;
    let queue = [0];
    let arr = [];
    let rootLen = root.length;
    
    while(queue.length > 0) {
        let len = queue.length;
        let tempArr = [];
        for (let i = 0; i < len; i++) {
            let index = queue.shift();
            let leftIndex = index * 2 + 1;
            let rightIndex = index * 2 + 2;

            if (root[index]) tempArr.push(root[index]);
            if (leftIndex < rootLen) queue.push(leftIndex);
            if (rightIndex < rootLen) queue.push(rightIndex);
        }
        arr.push(tempArr);
    }
    return arr.reverse();
};


function TreeNode(val) {
    this.val = val;
    this.left = this.right = null;
}

// console.log(levelOrderBottom([3,9,20,null,null,15,7]));
console.log(levelOrderBottom([3,9,20,null,null,15,7]));

