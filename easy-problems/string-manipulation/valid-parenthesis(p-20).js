

/**
 * @param {string} s
 * @return {boolean}
 * Given a string containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.

An input string is valid if:

Open brackets must be closed by the same type of brackets.
Open brackets must be closed in the correct order.
Note that an empty string is also considered valid.

Example 1:

Input: "()"
Output: true
Example 2:

Input: "()[]{}"
Output: true
Example 3:

Input: "(]"
Output: false
Example 4:

Input: "([)]"
Output: false
Example 5:

Input: "{[]}"
Output: true

Input: ""
Output: true
 */
var isValid = function(s) {
    let stack = [];

    if (s.length == 0) return false;

    for (let i = 0; i < s.length; i++) {
        let currentChar = s.charAt(i);

        if (currentChar == ')' || currentChar == '}' || currentChar == ']') {
            if(stack.length == 0) {
                return false;
            }
            topChar = stack.pop();
            if ((topChar == '(' && currentChar == ')') || (topChar == '{' && currentChar == '}') || (topChar == '[' && currentChar == ']')){
                continue;
            } else {
                return false;
            }
        } else {
            stack.push(currentChar);
        }
    }

    if (stack.length != 0) {
        return false;
    }
    return true;
};

console.log(isValid("()"));
console.log(isValid("()[]{}"));
console.log(isValid("(]"));
console.log(isValid(")"));
console.log(isValid("("));
console.log(isValid(""));
console.log(isValid("([)]"));
console.log(isValid(")]{}"));
console.log(isValid("[(())]{}"));


