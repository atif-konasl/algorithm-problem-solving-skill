/**
 * @param {ListNode} head
 * @return {ListNode}
 * Given a sorted linked list, delete all duplicates such that each element appear only once.

    Example 1:

    Input: 1->1->2
    Output: 1->2
    Example 2:

    Input: 1->1->2->3->3
    Output: 1->2->3
 */
var deleteDuplicates = function(head) {
    if (head == null) {
        return null;
    }
    let cur = head;
    let result = head;

    while (cur.next != null) {
        if (cur.val == cur.next.val) {
            cur.next = cur.next.next;
        } else {
            cur = cur.next;
        }
    }
    return result;
};

 function ListNode(val) {
    this.val = val;
    this.next = null;
}

let first = new ListNode(1);
let second = new ListNode(3);
let third = new ListNode(4);
let forth = new ListNode(5);
let fifth = new ListNode(6);

first.next = second;
second.next = third;
third.next = forth;
forth.next = fifth;


console.log(deleteDuplicates(first));