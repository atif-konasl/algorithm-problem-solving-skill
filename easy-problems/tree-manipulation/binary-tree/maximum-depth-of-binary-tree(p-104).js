/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number}
 * 
 * Given a binary tree, find its maximum depth.

The maximum depth is the number of nodes along the longest path from the root node down to the farthest leaf node.

Note: A leaf is a node with no children.

Example:

Given binary tree [3,9,20,null,null,15,7],

    3
   / \
  9  20
    /  \
   15   7
return its depth = 3.
 */

 //Solution using depth first search
var maxDepth_1 = function(root) {
    if (!root) {
        return 0;
    }
    return 1 + Math.max(maxDepth (root.left), maxDepth (root.right)); 
};

function TreeNode(val) {
    this.val = val;
    this.left = this.right = null;
}


//Solution using breadth first search
var maxDepth = function(root) {
    if (!root) {
        return 0;
    }
    let result = 0;
    let queue = [root];
    while (queue.length > 0) {
        result++;
        let len = queue.length;
        for (let i = 0; i < len; i++) {
            let node = queue.shift();
            if (node.left) queue.push(node.left);
            if (node.right) queue.push(node.right);
        }
    }
    return result;
};

let p = new TreeNode (3);
p.left = new TreeNode(9);
p.right = new TreeNode(20);
// p.left.right = new TreeNode(3);
// p.left.left = new TreeNode(4);
p.right.right = new TreeNode(15);
p.right.left = new TreeNode(7);

console.log(maxDepth(p));