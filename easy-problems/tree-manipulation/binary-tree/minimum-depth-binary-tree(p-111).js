/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number}
 * 
 * Given a binary tree, find its minimum depth.

The minimum depth is the number of nodes along the shortest path from the root node down to the nearest leaf node.

Note: A leaf is a node with no children.

Example:

Given binary tree [3,9,20,null,null,15,7],

    3
   / \
  9  20
    /  \
   15   7
return its minimum depth = 2.
 */

 /**
  * 
  * Approach - 1
  */
var minDepth_1 = function(root) {
    if (!root) return 0;
    return minHeight (root);
};

var minHeight = function (root) {
    if (!root.left && !root.right) {
        return 1;
    } 
    if (!root.left) {
        return 1 + minHeight (root.right);
    } 
    if (!root.right) {
        return 1 + minHeight (root.left)
    }
    return 1 + Math.min (minHeight (root.left), minHeight (root.right));
}

/**
 * 
 * Approach - 2
 */

var minDepth = function (root) {
    if (!root) return 0;
    let left = minDepth(root.left);
    let right = minDepth(root.right);
    return (left || right == 0) ? left + right + 1: Math.min(left,right) + 1;
}


function TreeNode (val) {
    this.val = val;
    this.left = this.right = null;
}

// Test cases
// let p = new TreeNode (3);
// p.right = new TreeNode (5);
// p.left = new TreeNode(9);
// p.left.right = new TreeNode(3);
// p.left.right.left = new TreeNode(15);
// p.left.right.left.left = new TreeNode(15);
// p.left.right.left.left.left = new TreeNode(15);

// Test Cases
// let p = new TreeNode (3);
// p.right = new TreeNode (5);
// p.left = new TreeNode(9);

// Test Cases
let p = new TreeNode (3);
p.left = new TreeNode (1);

// Test Cases
// let p = new TreeNode (1);

// Test Cases
// let p = new TreeNode (3);
// p.right = new TreeNode (5);
// p.right.right = new TreeNode (6);
// p.left = new TreeNode(9);
// p.left.left = new TreeNode (15);

// Test Cases
// let p = new TreeNode (1);
// p.left = new TreeNode (2);
// p.left.right = new TreeNode (3);
// // p.left.left = new TreeNode (10);
// p.left.right.left = new TreeNode(4);
// p.left.right.right = new TreeNode (5);
// p.left.right.right.left = new TreeNode (6);

console.log (minDepth (p));