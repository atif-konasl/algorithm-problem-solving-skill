/**
 * @param {number[]} nums
 * @param {number} val
 * @return {number}
 */
var removeElement_1 = function(nums, val) {
    for(let i = 0; i < nums.length; i++) {
        if (nums[i] == val) {
            nums.splice(i, 1);
            --i;
        }
    } 
    return nums.length; 
};
var removeElement_2 = function(nums, val) {
    while(nums.indexOf(val,0) >= 0) {
        console.log(nums.indexOf(val,0));
        nums.splice(nums.indexOf(val,0), 1);
    }
    return nums.length;
};


var removeElement_3 = function(nums, val) {
    let filtered = nums.filter( v => {
        return v != val;
    });
    console.log("filtered :" ,filtered);
    return (
        nums.splice(0, nums.length, ...nums.filter ( v => { 
                console.log(v);
                return v != val;
            })
        )
    )
};

console.log(removeElement_3([0,1,2,2,3,0,4,2], 2));
console.log(removeElement_3([2], 2));
console.log(removeElement_3([0], 2));