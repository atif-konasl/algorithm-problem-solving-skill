/**
 * initialize your data structure here.
 * 
 * Design a stack that supports push, pop, top, and retrieving the minimum element in constant time.

    push(x) -- Push element x onto stack.
    pop() -- Removes the element on top of the stack.
    top() -- Get the top element.
    getMin() -- Retrieve the minimum element in the stack.
    

    Example:

    MinStack minStack = new MinStack();
    minStack.push(-2);
    minStack.push(0);
    minStack.push(-3);
    minStack.getMin();   --> Returns -3.
    minStack.pop();
    minStack.top();      --> Returns 0.
    minStack.getMin();   --> Returns -2.
 */
//----------------------------- Approach-1 -------------------------------------//
var MinStack_1 = function() {
    this.stack = [];
    this.sortedStack = [];
};

/** 
 * @param {number} x
 * @return {void}
 */
MinStack_1.prototype.push = function(x) {
    this.stack.push (x);
    this.sort (x);
};

MinStack_1.prototype.sort = function (x) {
    let prevLen = this.sortedStack.length;
    for (let i = 0; i < prevLen; i++) {
        if (this.sortedStack [i] >= x) {
            this.sortedStack.splice (i, 0, x);
            break;
        }
    }
    if (this.sortedStack.length == prevLen) {
        this.sortedStack.splice (prevLen, 0, x);
    }
    // console.log ("Sorted stack : ", this.sortedStack);
}

/**
 * @return {void}
 */
MinStack_1.prototype.pop = function() {
    // console.log ("Before pop stack : ", this.stack);
    // console.log ("Before reOrder stack : ", this.sortedStack);
    if (this.stack.length > 0) {
        let x = this.stack [this.stack.length - 1];
        this.stack.pop ();
        this.reOrder (x);
        return x;
    } else return null;
};


MinStack_1.prototype.reOrder = function (x) {
    for (let i = 0; i < this.sortedStack.length; i++) {
        if (x == this.sortedStack[i]) {
            this.sortedStack.splice (i, 1);
        }
    }
    // console.log ("After reOrder stack : ", this.sortedStack);
}

/**
 * @return {number}
 */
MinStack_1.prototype.top = function() {
    return this.stack.length > 0 ? this.stack[this.stack.length - 1] : null;
};

/**
 * @return {number}
 */
MinStack_1.prototype.getMin = function() {
    return this.sortedStack.length > 0 ? this.sortedStack[0] : null;
};


//----------------------------- Approach-2 -------------------------------------//
var MinStack_2 = function() {
    this.stack = [];
    this.sortedStack = [];
};

/** 
 * @param {number} x
 * @return {void}
 */
MinStack_2.prototype.push = function(x) {
    this.stack.push (x);
    if (!this.sortedStack.length || (x <= this.getMin ())) {
        this.sortedStack.push (x);
    }
};

/**
 * @return {void}
 */
MinStack_2.prototype.pop = function() {
    if (this.top () == this.getMin ()) {
        this.sortedStack.pop ();
    }
    this.stack.pop ();
};

/**
 * @return {number}
 */
MinStack_2.prototype.top = function() {
    return this.stack.length > 0 ? this.stack[this.stack.length - 1] : null;
};

/**
 * @return {number}
 */
MinStack_2.prototype.getMin = function() {
    return this.sortedStack.length > 0 ? this.sortedStack[this.sortedStack.length - 1] : null;
};


//----------------------------- Approach-3 -------------------------------------//
var MinStack = function() {
    this.stack = [];
    this.min = Number.MAX_SAFE_INTEGER;
};

/** 
 * @param {number} x
 * @return {void}
 */
MinStack.prototype.push = function(x) {
    if (x <= this.min) {
        this.stack.push (this.min);
        this.min = x;
    }
    this.stack.push (x);
};

/**
 * @return {void}
 */
MinStack.prototype.pop = function() {
    if (this.stack.pop () == this.min) {
        this.min = this.top ();
        this.stack.pop ();
    }
};

/**
 * @return {number}
 */
MinStack.prototype.top = function() {
    return this.stack.length > 0 ? this.stack[this.stack.length - 1] : null;
};

/**
 * @return {number}
 */
MinStack.prototype.getMin = function() {
    return this.min;
};



/** 
 * Your MinStack object will be instantiated and called as such:
 * var obj = new MinStack()
 * obj.push(x)
 * obj.pop()
 * var param_3 = obj.top()
 * var param_4 = obj.getMin()
 */

 var obj = new MinStack ();
 obj.push (4);
 console.log (obj.stack);
 obj.push (9);
 console.log (obj.stack);
 obj.push (-1);
 console.log (obj.stack);
 obj.push (5);
 console.log (obj.stack);
 obj.pop ();
 console.log (obj.stack);
 obj.pop ();
 console.log (obj.stack);
 obj.push (12);
 console.log (obj.stack);
 obj.push (-1)
 console.log (obj.stack);
 obj.pop ();
 console.log (obj.stack);
 obj.pop ();
 console.log (obj.stack);
 obj.pop ();
 console.log (obj.stack);
 console.log (obj.top ());
 obj.pop ();
 console.log (obj.stack);
 
 