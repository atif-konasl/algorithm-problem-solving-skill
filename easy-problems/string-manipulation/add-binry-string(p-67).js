/**
 * @param {string} a
 * @param {string} b
 * @return {string}
 * 
 * Given two binary strings, return their sum (also a binary string).

    The input strings are both non-empty and contains only characters 1 or 0.

    Example 1:

    Input: a = "11", b = "1"
    Output: "100"
    Example 2:

    Input: a = "1010", b = "1011"
    Output: "10101"
 */
var addBinary = function(a, b) {
    let minLen = Math.min(a.length, b.length);
    // console.log(maxLen);
    let carry = 0;
    let result = '';
    for (let i = 0; i < minLen; i++) {
        if((a[i] == '0' && b[i] == '1') || (a[i] == '1' && b[i] == '0')) {
            carry == 1 ? result += '0' : result += '1';
        } else if (a[i] == 0 && b[i] == 0) {
            if (carry == 1) {
                result += '1';
                carry = 0;
            } else {
                result += '0';
            }
        } else {
            result += '0';
            carry = 1;
        }
    }
    console.log(result);
};

addBinary("1011", "10");