package com.jobIntreview.leetcode.array;

import com.jobIntreview.leetcode.LeetcodeApplicationTests;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestMediumArraySolution extends LeetcodeApplicationTests {
    @Autowired
    MediumArraySolution mediumArraySolution;

    @Test
    public void subsets() {
        int[] input_1 = {1,2,3};
        List<List<Integer>> expected_1 = new ArrayList<>();

        List<Integer> list8 = new ArrayList<>();
        expected_1.add(list8);

        List<Integer> list2 = new ArrayList<>();
        list2.add(1);
        expected_1.add(list2);

        List<Integer> list3 = new ArrayList<>();
        list3.add(2);
        expected_1.add(list3);

        List<Integer> list7 = new ArrayList<>();
        list7.add(1);
        list7.add(2);
        expected_1.add(list7);

        List<Integer> list1 = new ArrayList<>();
        list1.add(3);
        expected_1.add(list1);

        List<Integer> list5 = new ArrayList<>();
        list5.add(1);
        list5.add(3);
        expected_1.add(list5);

        List<Integer> list6 = new ArrayList<>();
        list6.add(2);
        list6.add(3);
        expected_1.add(list6);

        List<Integer> list4 = new ArrayList<>();
        list4.add(1);
        list4.add(2);
        list4.add(3);
        expected_1.add(list4);


        List<List<Integer>> actual_1 = mediumArraySolution.subsets_v1(input_1);
        assertEquals(expected_1, actual_1);
    }

    @Test
    public void rotate() {
        int[][] matrix_1 = {{1,2,3},{4,5,6},{7,8,9}};
        mediumArraySolution.rotate_v1(matrix_1);
        System.out.println();
        int[][] matrix_2 = {{5, 1, 9, 11},{2, 4, 8,10},{13, 3, 6, 7},{15, 14, 12, 16}};
        mediumArraySolution.rotate_v1(matrix_2);
    }

    @Test
    public void findDuplicate() {
        int[] nums_1 = {1,3,4,2,2};
        int actual_1 = 2;
        int expected_1 = mediumArraySolution.findDuplicate(nums_1);

        assertEquals(expected_1, actual_1);

        int[] nums_2 = {2,5,9,6,9,3,8,9,7,1};
        int actual_2 = 9;
        int expected_2 = mediumArraySolution.findDuplicate(nums_2);

        assertEquals(expected_2, actual_2);
    }


    @Test
    public void gameOfLife() {
        int[][] board_1 = {{0,1,0}, {0,0,1}, {1,1,1}, {0,0,0}};
        int[][] expected_1 = {{0,0,0}, {1,0,1}, {0,1,1}, {0,1,0}};
        int[][] actual_1 = mediumArraySolution.gameOfLife(board_1);

        assertArrayEquals(expected_1, actual_1);
    }

    @Test
    public void maxArea() {
        int[] heights_1 = {1,8,6,2,5,4,8,3,7};
        int expected_1 = 49;
        int actual_1 = mediumArraySolution.maxArea(heights_1);

        assertEquals(expected_1, actual_1);

        int[] heights_2 = {1,8,6,57,50,4,8,3,7};
        int expected_2 = 50;
        int actual_2 = mediumArraySolution.maxArea(heights_2);

        assertEquals(expected_2, actual_2);

        int[] heights_3 = {10, 0};
        int expected_3 = 0;
        int actual_3 = mediumArraySolution.maxArea(heights_3);

        assertEquals(expected_3, actual_3);
    }

    @Test
    public void buildTree() {
        int[] inorder_1 = {9,3,15,20,7};
        int[] preorder_1 = {3,9,20,15,7};
        TreeNode actual_1 = mediumArraySolution.buildTree(preorder_1, inorder_1);

        /**
         * Checking null
         */
        int[] inorder_2 = {};
        int[] preorder_2 = {};
        TreeNode actual_2 = mediumArraySolution.buildTree(preorder_2, inorder_2);

        /**
         * Checking single element
         */
        int[] inorder_3 = {1};
        int[] preorder_3 = {1};
        TreeNode actual_3 = mediumArraySolution.buildTree(preorder_3, inorder_3);

        /**
         * Here right sub-tree is null and
         * checking single element
         */
        int[] inorder_4 = {1,2};
        int[] preorder_4 = {2,1};
        TreeNode actual_4 = mediumArraySolution.buildTree(preorder_4, inorder_4);

        /**
         * Here left sub-tree is null
         */
        int[] inorder_5 = {1,2,3};
        int[] preorder_5 = {1,2,3};
        TreeNode actual_5 = mediumArraySolution.buildTree(preorder_5, inorder_5);

        /**
         * Here right sub-tree is null
         */
        int[] inorder_6 = {1,2,3};
        int[] preorder_6 = {3,2,1};
        TreeNode actual_6 = mediumArraySolution.buildTree(preorder_6, inorder_6);

//        /**
//         * [3,1,2,4]
//         * [1,2,3,4]
//         */
//        int[] inorder_7 = {3,1,2,4};
//        int[] preorder_7 = {1,2,3,4};
//        TreeNode actual_7 = mediumArraySolution.buildTree(preorder_7, inorder_7);
    }

}
