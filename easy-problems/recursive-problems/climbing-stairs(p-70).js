/**
 * @author atif anowar
 * @email atif.anowar@konasl.com
 * @create date 2019-09-07 12:10:31
 * @modify date 2019-09-07 12:10:31
 * @desc [description]
 */

 /**
 * @param {number} n
 * @return {number}
 * 
 * You are climbing a stair case. It takes n steps to reach to the top.

    Each time you can either climb 1 or 2 steps. In how many distinct ways can you climb to the top?

    Note: Given n will be a positive integer.

    Example 1:

    Input: 2
    Output: 2
    Explanation: There are two ways to climb to the top.
    1. 1 step + 1 step
    2. 2 steps
    Example 2:

    Input: 3
    Output: 3
    Explanation: There are three ways to climb to the top.
    1. 1 step + 1 step + 1 step
    2. 1 step + 2 steps
    3. 2 steps + 1 step
 */

/**
 * 
 * this is a recursive solution like getting solution 
 * of n=5 is ways(n) = ways(n - 1) + ways(n - 2) 
 */
var climbStairs = function(n) {
    if(n == 0) return 0;
    let firstStep = 1;
    let secondStep = 1;
    let nthResult = 0;
    for (let i = 2; i <= n; i++) {
        nthResult = firstStep + secondStep;
        secondStep = firstStep;
        firstStep = nthResult;
    }
    return n == 1 ? 1 : nthResult;
};

console.log(climbStairs(0));
console.log(climbStairs(1));
console.log(climbStairs(2));
console.log(climbStairs(4));
