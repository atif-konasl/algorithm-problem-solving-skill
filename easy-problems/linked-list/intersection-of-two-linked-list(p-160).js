/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */

/**
 * @param {ListNode} headA
 * @param {ListNode} headB
 * @return {ListNode}
 */
var getIntersectionNode = function(headA, headB) {

    if (!headA || !headB) return null;
    let currA = headA;
    let currB = headB;

    while (currA != currB) {
        currA = currA == null ? headA : currA.next;
        currB = currB == null ? headB : currB.next;
    }

    return currA;
};

function ListNode(val) {
    this.val = val;
    this.next = null;
}

// TestCase - 1
let headA = new ListNode (4);
let headB = new ListNode (5);
headA.next = new ListNode (1);
headA.next.next = new ListNode (8);
headA.next.next.next = headB.next = new ListNode (2);
headA.next.next.next.next = headB.next.next = new ListNode (4);

// // TestCase - 2
// let headA = new ListNode (0);
// let headB = new ListNode (3);
// headA.next = new ListNode (9);
// headA.next.next = new ListNode (1);

// TestCase - 3
// let headA = new ListNode (0);
// let headB = new ListNode (3);

console.log (getIntersectionNode (headA, headB));