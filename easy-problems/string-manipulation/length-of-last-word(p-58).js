/**
 * @param {string} s
 * @return {number}
 * Given a string s consists of upper/lower-case alphabets and empty space characters ' ', return the length of last word in the string.

    If the last word does not exist, return 0.

    Note: A word is defined as a character sequence consists of non-space characters only.

    Example:

    Input: "Hello World"
    Output: 5
 */
var lengthOfLastWord = function(s) {
    var regexConstructor = /\b(\w+)$/;
    let lastWord = s.match(regexConstructor);;
    if (lastWord == null) {
        return 0;
    }
    return lastWord[0].length;
};

var lengthOfLastWord = function(s) {
    return s.trim().split(" ").pop().length;
};

console.log(lengthOfLastWord("a"));
