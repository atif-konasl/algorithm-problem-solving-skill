/**
 * @param {string[]} strs
 * @return {string}
 * Write a function to find the longest common prefix string amongst an array of strings.

If there is no common prefix, return an empty string "".

Example 1:

Input: ["flower","flow","flight"]
Output: "fl"
Example 2:

Input: ["dog","racecar","car"]
Output: ""
Explanation: There is no common prefix among the input strings.
Note:

All given inputs are in lowercase letters a-z.
 */


var longestCommonPrefix_1 = function(strs) {
    let flag = true;
    let maxIndex = 0;

    while(flag) {
        if((strs.length == 0) || (maxIndex > strs[0].length - 1)) {
            maxIndex -= 1;
            flag = false;
            continue;
        }
        firstChar = strs[0].charAt(maxIndex);
        // console.log(firstChar, " flag : ", flag);
        for (let i = 1; i < strs.length; i++) {
            if((maxIndex > strs[i].length -1) || (firstChar != strs[i].charAt(maxIndex))) {
                maxIndex -= 1;
                flag = false;
                break;
            }
        }
        if(flag) {
            maxIndex += 1;
        }
    }
    // console.log(maxIndex);
    return maxIndex >= 0 ? strs[0].substring(0, maxIndex + 1) : "";
};

var longestCommonPrefix_2 = function(strs) {
    if (strs.length == 0 ) return "";
    let prefix = strs[0];
    for (let i = 1; i < strs.length; i++) {
        while (strs[i].indexOf(prefix) != 0) {
            prefix = prefix.substring(0, prefix.length - 1);
            if (prefix == "") return "";
        } 
    }
    return prefix;
}

// Approach 3: Divide and conquer
var longestCommonPrefix_3 = function(strs) {
    if (strs == null || strs.length == 0) return "";
    return divideAndConquer(strs, 0, strs.length - 1);
}

function divideAndConquer(strs, l, r) {
    if (l == r) return strs[l];
    else {
        let mid = parseInt((l + r) / 2);
        leftStr = divideAndConquer(strs, l, mid);
        rightStr = divideAndConquer(strs, mid + 1, r);

        return commonPrefix(leftStr, rightStr);
    }

}

function commonPrefix(leftStr, rightStr) {
    let min = Math.min(leftStr.length, rightStr.length);
    console.log(min);
    for (let i = 0; i < min; i++) {
        if (leftStr.charAt(i) != rightStr.charAt(i)) {
            return leftStr.substring(0, i);
        } 
    }
    return leftStr.substring(0, min);
}


// console.log(longestCommonPrefix_3(["flower","flow", "flight", "fuck", "fly"]));
console.log(longestCommonPrefix_3(["caa","","a","acb"]));
// console.log(longestCommonPrefix(["dog","racecar","car"]));
// console.log(longestCommonPrefix(["dog", "doggy", "dogma"]));
// console.log(longestCommonPrefix(["doggy", "dogma", "dog"]));
// console.log(longestCommonPrefix(["dog", "dog", "dog"]));
// console.log(longestCommonPrefix(["", "dog", "dog"]));
// console.log(longestCommonPrefix([]));
// console.log(longestCommonPrefix_2([""]));

