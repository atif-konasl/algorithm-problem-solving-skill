/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */

/**
 * @param {ListNode} head
 * @return {boolean}
 * 
 * Given a linked list, determine if it has a cycle in it.

To represent a cycle in the given linked list, we use an integer pos which represents the position (0-indexed) in the linked list where tail connects to. If pos is -1, then there is no cycle in the linked list.

 

Example 1:

Input: head = [3,2,0,-4], pos = 1
Output: true
Explanation: There is a cycle in the linked list, where tail connects to the second node.


Example 2:

Input: head = [1,2], pos = 0
Output: true
Explanation: There is a cycle in the linked list, where tail connects to the first node.


Example 3:

Input: head = [1], pos = -1
Output: false
Explanation: There is no cycle in the linked list.


 

Follow up:

Can you solve it using O(1) (i.e. constant) memory?


 */

 //---------------------- Approach - 1 -----------------------------------/
var hasCycle_1 = function(head) {    
    // var anyBoxesChecked = new Array(numeroPerguntas).fill(false);
    // var boolArray = new boolean[size];

    let visitedMap = new Map();
    let cur = head;

    while (cur) {
        visitedMap.set (cur, true);
        if (visitedMap.get (cur.next)) {
            return true;
        }
        cur = cur.next;
    }
    return false;
};


//---------------------- Approach - 2 -----------------------------------/
var hasCycle = function(head) {    
    let cur = head;

    while (cur) {
        if (cur.next && cur.next.val == null) {
            return true;
        }
        cur.val = null;
        cur = cur.next;
    }
    return false;
};

function ListNode (val) {
    this.val = val;
    this.next = null;
}

// Test Cases
// let p = new ListNode (3);
// p.next = new ListNode (2);
// p.next.next = new ListNode (0);
// p.next.next.next = new ListNode (-4);
// p.next.next.next.next = p.next;

// Test Cases
// let p = new ListNode (0);


console.log (hasCycle (p));