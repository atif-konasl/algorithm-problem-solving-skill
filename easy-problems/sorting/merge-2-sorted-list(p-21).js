/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */
/**
 * @param {ListNode} l1
 * @param {ListNode} l2
 * @return {ListNode}
 * 
 * Merge two sorted linked lists and return it as a new list. The new list should be made by splicing together the nodes of the first two lists.

    Example:

    Input: 1->2->4, 1->3->4
    Output: 1->1->2->3->4->4

 */
var mergeTwoLists = function(l1, l2) {
    let head = new ListNode(0);
    let cur1 = l1;
    let cur2 = l2;
    let cur = head;
    while(cur1 != null && cur2 != null) {
        // console.log("cur1 : ", cur1.val, " cur2 : ", cur2.val, " cur : ", cur);
        if (cur1.val <= cur2.val) {
            cur.next = new ListNode(cur1.val);
            cur1 = cur1.next;
        } else {
            cur.next = new ListNode(cur2.val);
            cur2 = cur2.next;
        }
        cur = cur.next;
    }
    if (cur1 != null) {
        cur.next = cur1;
    } 
    if (cur2 != null) {
        cur.next = cur2;
    }
    head = head.next;
    // printList(head);
    return head;
};

function ListNode(val) {
    this.val = val;
    this.next = null;
}

function printList(x1) {
    let cur = x1;
    while(cur != null) {
        console.log(cur.val);
        cur = cur.next;
    }
}

let x1 = new ListNode();
// let x2 = new ListNode(2);
// let x3 = new ListNode(4);

let y1 = new ListNode(-20);
let y2 = new ListNode(3);
let y3 = new ListNode(4);

// x1.next = x2;
// x2.next = x3;
y1.next = y2;
y2.next = y3;

// console.log("Print x :")
// printList(x1);
// console.log("Print y :")
// printList(y1);
// console.log("Print merge sort :")
mergeTwoLists(x1, y1);
