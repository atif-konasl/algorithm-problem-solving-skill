/**
 * @author atif anowar
 * @email atif.anowar@konasl.com
 * @create date 2019-09-07 10:31:41
 * @modify date 2019-09-07 10:31:41
 * @desc [description]
 */
/**
 * @param {string} a
 * @param {string} b
 * @return {string}
 * Given two binary strings, return their sum (also a binary string).

    The input strings are both non-empty and contains only characters 1 or 0.

    Example 1:

    Input: a = "11", b = "1"
    Output: "100"
    Example 2:

    Input: a = "1010", b = "1011"
    Output: "10101"
 */
var addBinary_1 = function(a, b) {
    let revA = a.split("").reverse().join("");
    let revB = b.split("").reverse().join("");
    // console.log(revA + '  ' + revB);
    let maxLen = Math.max(a.length, b.length);
    let carry = 0;
    let result = '';
    for (let i = 0; i < maxLen; i++) {
        if ((i < a.length) && (i < b.length)) {
            // console.log(i + '  ' + revA[i] + '  ' + revB[i]  + '  ' + carry);
            if ((revA[i] == '0' && revB[i] == '1') || (revA[i] == '1' && revB[i] == '0')) {
                carry == 1 ? result += '0' : result += '1';
            } else if (revA[i] == '0' && revB[i] == '0') {
                if (carry == 1) {
                    result += '1';
                    carry = 0;
                } else {
                    result += '0';
                }
                
            } else {
                carry == 1 ? result += '1' : result += '0';
                carry = 1;
            }
        } else if ((i >= a.length) && (i < b.length)) {
            if (revB[i] == 0) {
                carry == 1 ? result += '1' : result += '0';
                carry = 0;
            } else {
                carry == 1 ? result += '0' : result += '1';
            }
        } else {
            if (revA[i] == 0) {
                carry == 1 ? result += '1' : result += '0';
                carry = 0;
            } else {
                carry == 1 ? result += '0' : result += '1';
            }
        }
    }
    carry == 1 ? result += '1' : result += '0';
    result = result.split('').reverse().join('');
    if (parseInt(result) == 0) return '0';
    return result.replace(/^0+/, '');
};

var addBinary = function(a, b) {
    let binA = a.toString(10);
    let binB = b.toString(10);
    console.log(binA + '     ' + binB)
    // let sum = binA + binB;
    // sum = sum.toString(2);
    // return sum;
}

var addBinary = function(a, b) {
    a = a.split("").reverse().join("");
    b = b.split("").reverse().join("");
    len = a.length > b.length ? a.length : b.length;
    result = [];
    for(let i = 0; i < len; i += 1){
        num1 = Number(a[i] || 0);
        num2 = Number(b[i]) || 0;
        curr = Number(result[i]||0) + num1 + num2
        if(curr >= 2){
            result[i] = curr%2;
            result.push(1)
        }
        else{
            result[i] = curr
        }
    }
    return result.reverse().join("")
};

console.log(addBinary("10100000100100110110010000010101111011011001101110111111111101000000101111001110001111100001101",
"110101001011101110001111100110001010100001101011101010000011011011001011101111001100000011011110011"));   
// 110111101100010011000101110110100000011101000101011001000011011000001100011110011010010011000000000
// 110111101100010011000101110110100000011101000101011000000000000000000000000000000000000000000000000
// console.log(addBinary('1010', '1011'));
// console.log(addBinary('1111', '1'));
// console.log(addBinary('1', '0'));
// console.log(addBinary('0', '0'));