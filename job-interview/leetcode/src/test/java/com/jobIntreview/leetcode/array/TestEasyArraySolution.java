package com.jobIntreview.leetcode.array;

import com.jobIntreview.leetcode.LeetcodeApplicationTests;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestEasyArraySolution extends LeetcodeApplicationTests {

    @Autowired
    EasyArraySolution easyArraySolution;

    @Test
    public void moveZeros() {
        int [] input_1 = {0,1,0,3,12};
        int [] expected_1 = {1,3,12,0,0};
        int [] actual_1 = easyArraySolution.moveZeroes(input_1);
        assertArrayEquals(expected_1, actual_1);

        int [] input_2 = {0,0,0,0,12};
        int [] expected_2 = {12,0,0,0,0};
        int [] actual_2 = easyArraySolution.moveZeroes(input_2);
        assertArrayEquals(expected_2, actual_2);

        int [] input_3 = {1,2,3,5,12};
        int [] expected_3 = {1,2,3,5,12};
        int [] actual_3 = easyArraySolution.moveZeroes(input_3);
        assertArrayEquals(expected_3, actual_3);

        int [] input_4 = {1,2,3,5,0};
        int [] expected_4 = {1,2,3,5,0};
        int [] actual_4 = easyArraySolution.moveZeroes(input_4);
        assertArrayEquals(expected_4, actual_4);
    }

    @Test
    public void majorityElement() {
        int [] input_1 = {3, 2, 3};
        int expected_1 = 3;
        int actual_1 = easyArraySolution.majorityElement(input_1);
        assertEquals(expected_1, actual_1);

        int [] input_2 = {2,2,1,1,1,2,2};
        int expected_2 = 2;
        int actual_2 = easyArraySolution.majorityElement(input_2);
        assertEquals(expected_2, actual_2);

        int [] input_3 = {2,1};
        int expected_3 = 2;
        int actual_3 = easyArraySolution.majorityElement(input_3);
        assertEquals(expected_3, actual_3);
    }

    @Test
    public void containsDuplicate() {
        int [] input_1 = {1, 2, 3};
        boolean expected_1 = false;
        boolean actual_1 = easyArraySolution.containsDuplicate(input_1);
        assertEquals(expected_1, actual_1);

        int [] input_2 = {1, 2, 3, 1};
        boolean expected_2 = true;
        boolean actual_2 = easyArraySolution.containsDuplicate(input_2);
        assertEquals(expected_2, actual_2);
    }

    @Test
    public void maxProfit() {
        int [] input_1 = {1};
        int expected_1 = 0;
        int actual_1 = easyArraySolution.maxProfit(input_1);
        assertEquals(expected_1, actual_1);

        int [] input_2 = {1, 7, 2, 3, 6, 7, 6, 7};
        int expected_2 = 12;
        int actual_2 = easyArraySolution.maxProfit(input_2);
        assertEquals(expected_2, actual_2);
    }

    @Test
    public void missingNumber_v1() {
        int[] input_1 = {3,0,1};
        int expected_1 = 2;
        int actual_1 = easyArraySolution.missingNumber_v1(input_1);
        assertEquals(expected_1, actual_1);

        int[] input_2 = {9,6,4,2,3,5,7,0,1};
        int expected_2 = 8;
        int actual_2 = easyArraySolution.missingNumber_v1(input_2);
        assertEquals(expected_2, actual_2);

        int[] input_3 = {0};
        int expected_3 = 1;
        int actual_3 = easyArraySolution.missingNumber_v1(input_3);
        assertEquals(expected_3, actual_3);

    }

    @Test
    public void missingNumber_v2() {
        int[] input_1 = {3,0,1};
        int expected_1 = 2;
        int actual_1 = easyArraySolution.missingNumber_v2(input_1);
        assertEquals(expected_1, actual_1);

        int[] input_2 = {9,6,4,2,3,5,7,0,1};
        int expected_2 = 8;
        int actual_2 = easyArraySolution.missingNumber_v2(input_2);
        assertEquals(expected_2, actual_2);

        int[] input_3 = {0};
        int expected_3 = 1;
        int actual_3 = easyArraySolution.missingNumber_v2(input_3);
        assertEquals(expected_3, actual_3);

    }

}
