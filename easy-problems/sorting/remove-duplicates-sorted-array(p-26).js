/**
 * @param {number[]} nums
 * @return {number}
 */
var removeDuplicates = function(nums) {
    for (let i = 0; i < nums.length - 1; i++) {
        let start = i;
        let duplicateCounts = 0;
        while ((i < nums.length - 1) && (nums[i] == nums[i + 1])) {
            duplicateCounts++;
            i++;
        }
        // console.log(duplicateCounts, "   ", i);
        if(duplicateCounts > 0) {
            nums.splice(start + 1, duplicateCounts);
            i = start;
        }
        // console.log(nums)
    }
    // console.log(nums);
    return nums.length  
};


console.log(removeDuplicates([0,0,1,1,1,2,2,3,3,4]));
console.log(removeDuplicates([0,1,3,4]));
console.log(removeDuplicates([]));
console.log(removeDuplicates([0, 1, 3, 4, 4, 4]));