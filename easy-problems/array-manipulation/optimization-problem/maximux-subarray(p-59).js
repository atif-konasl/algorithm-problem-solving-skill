/**
 * @param {number[]} nums
 * @return {number}
 * Given an integer array nums, find the contiguous subarray (containing at least one number) which has the largest sum and return its sum.

    Example:

    Input: [-2,1,-3,4,-1,2,1,-5,4],
    Output: 6
    Explanation: [4,-1,2,1] has the largest sum = 6.
    Follow up:

    If you have figured out the O(n) solution, try coding another solution using the divide and conquer approach, which is more subtle.
 */
var maxSubArray = function(nums) {
    let maxSoFar = nums[0];
    let sum = nums[0];

    for (let i = 1; i < nums.length; i++) {
       sum = (sum > 0 ? sum : 0) + nums[i];
       if (maxSoFar < sum) {
          maxSoFar = sum;
       }
    }
    return maxSoFar;
};

// console.log(maxSubArray([-2,1,-3,4,-1,2,1,-5,4]));
// console.log(maxSubArray([-1, -3, -5]));
console.log(maxSubArray([-1, -3, 3, -5, 5]));

