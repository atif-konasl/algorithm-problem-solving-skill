/**
 * @param {number} x
 * @return {number}
 * Problem-description : Given a 32-bit signed integer, reverse digits of an integer.

    Example 1:

    Input: 123
    Output: 321
    Example 2:

    Input: -123
    Output: -321
    Example 3:

    Input: 120
    Output: 21
    Note:
    Assume we are dealing with an environment which could only store integers within the 32-bit signed integer range: [−231,  231 − 1]. For the purpose of this problem, assume that your function returns 0 when the reversed integer overflows.
 */
var reverse_1 = function(x) {
    
    let rev = 0;
    let input = x;
    const MAX_INT = Math.pow(2, 31) - 1;
    const MIN_INT = Math.pow(-2, 31);


   
    if (input < 0) {
        input = input * -1;
    }
    while( input != 0 ) {
        let d = input % 10;
        input = parseInt(input / 10);
        rev = rev * 10 + d;
    }
    if(x < 0 ) {
        rev = rev * -1;
    }

    if(rev < MIN_INT || rev > MAX_INT) {
        return 0;
    }
    return rev;
};


var reverse_2 = function(x) {
    if ((x < -1 * (2 ** 31)) || (x > ((2 ** 31) -1))) return 0;
    let result = (x < 0 ? -1 : 1) * Number(String(Math.abs(x)).split('').reverse().join(''));
    if (result < Math.pow(-2, 31) || result > Math.pow(2, 31) - 1) return 0;
    return result;
}




console.log(reverse_2(-1245));
console.log(reverse_2(-0));
console.log(reverse_1(-10));
console.log(reverse_1(10));
console.log(reverse_2(1534236469));