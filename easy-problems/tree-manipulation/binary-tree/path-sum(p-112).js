/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @param {number} sum
 * @return {boolean}
 * 
 Given a binary tree and a sum, determine if the tree has a root-to-leaf path such that adding up all the values along the path equals the given sum.

Note: A leaf is a node with no children.

Example:

Given the below binary tree and sum = 22,

      5
     / \
    4   8
   /   / \
  11  13  4
 /  \      \
7    2      1
return true, as there exist a root-to-leaf path 5->4->11->2 which sum is 22.
 */

 //-----------------------Approach -1 -----------------------//

var hasPathSum_1 = function(root, sum) {
    if (!root) return false;
    let queue = [root];
    while (queue.length > 0) {
        let cur = queue.shift();
        if (!cur.left && !cur.right && cur.val == sum) {
            return true;
        }
        if (cur.left) {
            cur.left.val += cur.val;
            queue.push (cur.left);
        }
        if (cur.right) {
            cur.right.val += cur.val;
            queue.push (cur.right);
        }
    }
    return false;
};

//-----------------Approach-2--------//
var hasPathSum = function(root, sum) {
    if (!root) return false;
    if (!root.left && !root.right && sum - root.val == 0) return true;
    
    return hasPathSum (root.left, sum - root.val) || hasPathSum (root.right, sum - root.val);
}

function TreeNode (val) {
    this.val = val;
    this.left = this.right = null;
}


// Test Cases
let p = new TreeNode (5);
p.left = new TreeNode(4);
p.right = new TreeNode (8);
p.left.left = new TreeNode (11);
p.left.left.left = new TreeNode (7);
p.left.left.right = new TreeNode (2);
p.right.left = new TreeNode (13);
p.right.right = new TreeNode (4);
p.right.right.right = new TreeNode (1);

// Test Cases
// let p = new TreeNode (5);

console.log (hasPathSum (null, 0));